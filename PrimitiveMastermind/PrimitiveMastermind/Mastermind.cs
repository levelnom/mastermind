﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimitiveMastermind
{
    internal class Mastermind
    {
        //this will be our board
        private int[] board;

        //create the board at class creation with deafualt size of four
        public Mastermind(int size = 4)
        {
            board = new int[size];
            Random random = new Random();

            //loop through the board and populate with numbers between 1 and 6
            for (int i = 0; i < size; i++)
            {
                board[i] = random.Next(1, 6);
            }
        }

        public String checkBoard(int[] playerGuess)
        {
            //data validation
            if (playerGuess == null || playerGuess.Length != board.Length)
            {
                return "Invalid data";
            }

            String result = "";

            //create an array to store any matches to be skipped over durring the "correct number but wrong place" search
            Boolean[] skip = new Boolean[board.Length];

            //for each one in the board
            for (int i = 0; i < board.Length; i++)
            {
                //if it matches, add "+" to the string
                if (board[i] == playerGuess[i])
                {
                    result += "+";
                    skip[i] = true;
                }
                else
                {
                    for (int j = 0; j < board.Length; j++)
                    {
                        //check if it matches else where BUT only when that number is not already matched up
                        //correct location and number should not be counted
                        if (j != i && !skip[j] && board[j] != playerGuess[j] && board[j] == playerGuess[i])
                        {
                            result += "-";
                            skip[j] = true;
                            break;
                        }
                    }
                }
            }


            return result;
        }
        public String getNumber()
        {
            String result = "";
            foreach (int i in board)
            {
                result += i;
            }

            return result;
        }
    }
}
