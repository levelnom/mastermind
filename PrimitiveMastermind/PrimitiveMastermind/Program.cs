﻿PrimitiveMastermind.Mastermind mastermind = new PrimitiveMastermind.Mastermind();

int[] playerGuess = new int[4];
String result = "";

for (int i = 0; i < 10; i++)
{
    Console.WriteLine("Type a number then press enter four times");
    for (int j = 0; j < playerGuess.Length; j++)
    {
        try
        {
            playerGuess[j] = int.Parse(Console.ReadLine());
        } catch {
            Console.WriteLine("Incorrect value");
            j--;
        }
    }
    result = mastermind.checkBoard(playerGuess);

    if (result.Equals("++++")){
        Console.WriteLine("You Win!");
        Environment.Exit(0);
    }
    else if(!result.Equals(""))
    {
        Console.WriteLine("Wrong. Here is a hint: " + result);
    }
    else
    {
        Console.WriteLine("Wrong. Here is a hint: none of your numbers is correct place or value");
    }
}
Console.WriteLine("Game Over. The number were " + mastermind.getNumber());
